package mailhenvendelser;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;


import com.nykredit.kundeservice.data.CTIRConnection;

public class MOracle extends CTIRConnection{


	public MOracle(String program){
		try{
			Connect();
		}catch(SQLException e){e.printStackTrace();}
		SetupSpy(program);
	}


	public ResultSet getAverage(String start, String slut, boolean isSunday){
		SS_spy();
		if (!isSunday && calculateDaysBetween(start, slut) > 1) {
			DateTime d = new DateTime();
			d.minusDays(1);
			DateTime newSlut = new DateTime(slut);
			if (newSlut.isAfter(d)) {
				newSlut = d;	
			}
			newSlut = newSlut.withFieldAdded(DurationFieldType.days(), -2);
			slut = newSlut.getYear() +"-"+newSlut.getMonthOfYear()+"-"+newSlut.getDayOfMonth();

		}
		DateTime d = new DateTime();
		d.minusDays(1);
		DateTime newSlut = new DateTime(slut);
		if (newSlut.isAfter(d)) {
			newSlut = d;
			newSlut = newSlut.withFieldAdded(DurationFieldType.days(), -2);
			slut = newSlut.getYear() +"-"+newSlut.getMonthOfYear()+"-"+newSlut.getDayOfMonth();

		}
		int devisor = calculateDaysBetween(start, slut);
		System.out.println(devisor);

		try{
			return Hent_tabel(
					"SELECT                                     " +
							"   to_char(FREMRYKKET_OPRETTET,'HH24')||':00' AS KL,  " +
							"    ROUND(COUNT(FREMRYKKET_OPRETTET) /"+devisor+",5) as B9_IND				" +     
							"FROM     KS_DRIFT.V_EMAIL_RENS      " +
							"WHERE                                      " +
							"   TRUNC(DATO)>='" + start + "'         " +
							"   and TRUNC(DATO)<='" + slut + "'      " +
							"GROUP BY                                   " +
							"   TO_CHAR(FREMRYKKET_OPRETTET,'HH24')                 " +
							"ORDER BY 									" +
					"	 TO_CHAR(FREMRYKKET_OPRETTET,'HH24') 				");	
		}catch (SQLException e){e.printStackTrace();}
		return null;
	}

	public ResultSet getTotals(String start, String slut, boolean isSunday){
		SS_spy();
		System.out.println(isSunday);
		calculateDaysBetween(start, slut);
		if (!isSunday && calculateDaysBetween(start, slut) > 1 ) {
			DateTime d = new DateTime();
			d.minusDays(1);
			DateTime newSlut = new DateTime(slut);
			if (newSlut.isAfter(d)) {
				newSlut = d;	
			}
			newSlut = newSlut.withFieldAdded(DurationFieldType.days(), -2);
			slut = newSlut.getYear() +"-"+newSlut.getMonthOfYear()+"-"+newSlut.getDayOfMonth();

		}
		System.out.println(slut);
		try{
			return Hent_tabel(
					"SELECT                                     " +
							"   to_char(FREMRYKKET_OPRETTET,'HH24')||':00' AS KL,  " +
							"    COUNT(FREMRYKKET_OPRETTET) as B9_IND				" +     
							"FROM     KS_DRIFT.V_EMAIL_RENS      " +
							"WHERE                                      " +
							"   TRUNC(DATO)>='" + start + "'         " +
							"   and TRUNC(DATO)<='" + slut + "'      " +
							"GROUP BY                                   " +
							"   TO_CHAR(FREMRYKKET_OPRETTET,'HH24')                 " +
							"ORDER BY 									" +
					"	 TO_CHAR(FREMRYKKET_OPRETTET,'HH24') 				");

		}catch (SQLException e){e.printStackTrace();}
		return null;
	}
	private int calculateDaysBetween(String start, String slut){
		org.joda.time.DateTime startdate = new org.joda.time.DateTime(start);
		DateTime endDate = new DateTime(slut);
		return 	Days.daysBetween(startdate.toDateMidnight(), endDate.toDateMidnight()).getDays() + 1;


	}

}
