package mailhenvendelser;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.nykredit.kundeservice.swing.NTableRenderer;




public class MTableRenderer extends NTableRenderer {
 private static final long serialVersionUID = 1L;

 public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
	 //setZebraColor(new Color(0,255,0), new Color(255,0,0));
	 Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
    

  if (column > 0)
  	setHorizontalAlignment(SwingConstants.CENTER);
  else
	setHorizontalAlignment(SwingConstants.LEFT);

  
  	addMarking(cell,isSelected,new Color(0,0,128),80);

  	
  
  
     return cell;
 }
}
