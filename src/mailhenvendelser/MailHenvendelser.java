package mailhenvendelser;

import com.nykredit.kundeservice.swing.DateSelector;
import com.nykredit.kundeservice.swing.NFrame;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;


import org.jfree.chart.ChartPanel;

public class MailHenvendelser extends NFrame
{
	private static final long serialVersionUID = 1L;
	private MChartPanel mChartPanel;
	private JDesktopPane mainPane = null;
	private JLabel labelCopyright;
	private JScrollPane tableScrollPane = null;
	private JButton buttonGetData = null;
	private MailTable mailTable = null;
	private JCheckBox checkBoxSunday = null;
	private DateSelector dateSelector = new DateSelector(10, 10, 200, 50);
	private MOracle mOracle;
	private String program;
	private JRadioButton snit;
	private JRadioButton total;
	private ButtonGroup buttonGroup = new ButtonGroup();

	private JScrollPane getTableScrollPane()
	{
		this.tableScrollPane = new JScrollPane();
		this.tableScrollPane.setBounds(new Rectangle(10, 350, 600, 85));
		this.tableScrollPane.setViewportView(getMailTable());
		this.tableScrollPane.setBorder(BorderFactory.createLineBorder(Color.black));
		return this.tableScrollPane;
	}

	private MailTable getMailTable() {
		this.mailTable = new MailTable(this.mOracle);
		return this.mailTable;
	}

	private ChartPanel getMChartPanel()
	{
		this.mChartPanel = new MChartPanel(null, this.mOracle);
		this.mChartPanel.setBounds(new Rectangle(10, 80, 600, 260));
		return this.mChartPanel;
	}

	private JButton getJButton()
	{
		this.buttonGetData = new JButton("Hent data");
		this.buttonGetData.setBounds(new Rectangle(230, 10, 110, 30));
		setLocationRelativeTo(getRootPane());
		this.buttonGetData.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				MailHenvendelser.this.mailTable.LoadData(MailHenvendelser.this.dateSelector.getStart(), MailHenvendelser.this.dateSelector.getEnd(), MailHenvendelser.this.checkBoxSunday.isSelected(), MailHenvendelser.this.snit.isSelected());
				MailHenvendelser.this.mChartPanel.loadGraphData(MailHenvendelser.this.dateSelector.getStart(), MailHenvendelser.this.dateSelector.getEnd(), MailHenvendelser.this.checkBoxSunday.isSelected(), MailHenvendelser.this.dateSelector.getPeriode(), MailHenvendelser.this.snit.isSelected());
			}
		});
		return this.buttonGetData;
	}

	private JCheckBox getCheckBoxSunday() {
		this.checkBoxSunday = new JCheckBox("Inklud�r weekend");
		this.checkBoxSunday.setBounds(new Rectangle(230, 40, 120, 26));
		return this.checkBoxSunday;
	}

	public MailHenvendelser(String program) {
		this.program = program;
		this.mOracle = new MOracle(program);
		initialize();
	}

	public void initialize()
	{
		setSize(630, 500);
		setContentPane(getJDesktopPane());
		setTitle(this.program);
		/*addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we) {
				Object[] options = { "Ja", "Nej" };
				int confirmed = JOptionPane.showOptionDialog(MailHenvendelser.this, 
						"Er du sikker p� du vil lukke programmet?", "Afslut program", 
						0, 
						3, 
						null, 
						options, 
						options[0]);

				if (confirmed == 0) System.exit(0);
			}
		});
		*/
		setResizable(false);
		setVisible(true);
	}

	private JRadioButton getSnit() {
		this.snit = new JRadioButton("Hvis gennemsnitlige henvendelser");
		this.snit.setBounds(new Rectangle(350, 10, 300, 20));
		this.snit.setSelected(true);
		this.buttonGroup.add(this.snit);
		return this.snit;
	}

	private JRadioButton getTotal() {
		this.total = new JRadioButton("Hvis totale henvendelser");
		this.total.setBounds(new Rectangle(350, 30, 300, 20));
		this.buttonGroup.add(this.total);
		return this.total;
	}

	private JDesktopPane getJDesktopPane() {
		this.mainPane = new JDesktopPane();
		this.mainPane.setOpaque(false);
		this.labelCopyright = new JLabel("DOPE � Nykredit");
		this.labelCopyright.setFont(new Font("Dialog", 0, 10));
		this.labelCopyright.setBounds(new Rectangle(10, 440, 85, 16));
		this.mainPane.add(this.dateSelector);
		this.mainPane.add(this.labelCopyright);
		this.mainPane.add(getJButton());
		this.mainPane.add(getTableScrollPane());
		this.mainPane.add(getCheckBoxSunday());
		this.mainPane.add(getMChartPanel());
		this.mainPane.add(getSnit());
		this.mainPane.add(getTotal());
		return this.mainPane;
	}
}
