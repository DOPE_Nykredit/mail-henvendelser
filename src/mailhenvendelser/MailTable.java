package mailhenvendelser;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.swing.NTable;

public class MailTable extends NTable {

	private static final long serialVersionUID = 1L;
	private MOracle mOracle;
	private DefaultTableModel model;
	private Vector<String> 
	b9ind,
	b9fejl,
	b2b8ind,
	b2b8fejl;
	public MailTable(MOracle mOracle){
		this.mOracle = mOracle;
		setupModel();
	}

	private void setupModel(){

		model = new DefaultTableModel();
		setModel(model);
		setDefaultRenderer(Object.class, new MTableRenderer());

		b9ind = new Vector<String>();
		b9fejl = new Vector<String>();
		b2b8ind = new Vector<String>();
		b2b8fejl = new Vector<String>();

		model.addColumn("Kl.");
		b9ind.add("Ms total");
		b9fejl.add("MS overskr");
		b2b8ind.add("B1-7 total");
		b2b8fejl.add("B1-7 overskr");

		//getColumnModel().getColumn(1).setCellRenderer(new MTableRenderer());
		getTableHeader().setReorderingAllowed(false);
		setColumnSelectionAllowed(false);
		setRowSelectionAllowed(false);
		setCellSelectionEnabled(true);
		getTableHeader().setFont(new Font("Arial",Font.PLAIN,9));
		setFont(new Font("Arial",Font.PLAIN,10));
	}

	public void LoadData(String start,String slut,boolean isSunday,boolean snit){

		setupModel();

		try {

			ResultSet rs;
			if (snit){
				rs = mOracle.getAverage(start,slut,isSunday);
			}else{
				rs = mOracle.getTotals(start,slut,isSunday);
			}
			double b9indtotal = 0;
			while(rs.next()) {
				model.addColumn(rs.getString("KL"));
				double gg = Math.round(rs.getDouble("B9_IND"));
				b9ind.add(""+(int)gg); 
				System.out.println(rs.getDouble("B9_IND"));
				b9indtotal +=rs.getDouble("B9_IND");
				/*	b9fejl.add(rs.getString("B9_FEJL")); b9fejltotal = b9fejltotal + rs.getInt("B9_FEJL");
	        	b2b8ind.add(rs.getString("B2B8_IND")); b2b8indtotal = b2b8indtotal + rs.getInt("B2B8_IND");
				 *b2b8fejl.add(rs.getString("B2B8_FEJL")); b2b8fejltotal = b2b8fejltotal + rs.getInt("B2B8_FEJL");
				 */}
			b9indtotal = Math.round(b9indtotal);
			model.addColumn("Total");
			b9ind.add((int)b9indtotal + "");
			model.addRow(b9ind);

		} catch (SQLException e1) {e1.printStackTrace();}	
		getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(120);
	}

	public boolean isCellEditable(int rowIndex, int colIndex) {
		return false;
	}

}
