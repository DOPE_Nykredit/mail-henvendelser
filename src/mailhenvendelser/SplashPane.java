package mailhenvendelser;


import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class SplashPane extends JPanel{

	private static final long serialVersionUID = 1L;
	private BufferedImage splash = null;
	
	SplashPane(){
		
		try {
			splash = ImageIO.read(this.getClass().getResource("splash.jpg"));
		} catch (Exception e) {System.out.println(e);}
	}
	
	@Override
    public void paintComponent(Graphics g) {
        g.drawImage(splash, 0, 0, null); 
    }



}
